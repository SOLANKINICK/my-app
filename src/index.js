import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'
import AppComponent from './App.js';

ReactDOM.render(<AppComponent />, document.getElementById("app"));
