import React, { Component } from 'react';
import Header from './Header.js';
import Clock from './Clock.js';

export default class App extends Component{
    constructor(){
    super();
    this.state = {title:'React-Clock'};
    }

    render(){
        return(
            <div>
                <Header title={this.state.title} />;
                <Clock />;
            </div>
        );
    }
}