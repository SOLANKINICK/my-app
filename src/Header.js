import React from 'react';
import PropTypes from 'prop-types';

const Header = (props)=>(
    <div className='"header navbar navbar-dark bg-dark'>
        <div className='row m-auto'>
        <i className="fa fa fa-clock-o fa-4x text-white"></i>
        <div className="h1 ml-3 my-auto text-light">{props.title}</div>
        </div>

    </div>
);

Header.defaultProps = {
    title: 'Title'
};

Header.propTypes = {
 title : PropTypes.string
};

export default Header;
