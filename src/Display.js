import React from 'react';
import PropTypes from 'prop-types';


const Display = (props)=>{
    return(
        <div className="container-fluid">
            <div className="d-flex flex-row">
                <div className="col-md-4 mx-auto">
                    <div className="display">
                        <div className="display-Time">{props.date.toLocaleTimeString()}</div>
                        {props.isDateVisible && <div className="display-Date">{props.date.toLocaleDateString()}</div>}
                    </div>
                </div>
            </div>
        </div>
    );
};

Display.defaultProps = {
    date: new Date(),
    isDateVisible: true
};

Display.propTypes = {
    date: PropTypes.object,
    isDateVisible: PropTypes.bool
};

export default Display;
